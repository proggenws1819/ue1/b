public class JavaCode {

    public static void main(String[] args) {
        float f1 = 1000.0F;
        float f2 = 0.00003F;
        float f3 = f1 + f2 + f2 + f2 + f2;
        float f4 = f2 + f2 + f2 + f2 + f1;
        boolean w = Math.abs(f3 - f4) < 0.001;
        byte a = 23;
        byte b = 42;
        byte c = (byte) (a+b);
        int id = 999999999;
        System.out.println(id);
        System.out.println(System.getProperty("java.version"));
    }
}
